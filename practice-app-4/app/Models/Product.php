<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'create_at',
        'update_at'
    ];
}
